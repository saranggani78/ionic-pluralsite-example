
  'use strict';

  angular.module('eliteApp')

  // using custom service "eliteApi"
  .controller('LeaguesCtrl', function($state,eliteApi){
    var data = this;
    // var leagueData = eliteApi.getLeagueData();
    var leagues = eliteApi.getLeagues();
    // console.log("leagueData",leagueData);
    data.leagues = leagues;
    data.selectLeague = function(lid){
      // query data by league id to get correct league
      console.log("league id: ", lid);
      $state.go("app.teams");
    };
  });