
  'use strict';

  angular.module('eliteApp')

  // using custom service "eliteApi"
  .controller('StandingsCtrl', function(eliteApi){
    var data = this;
    var leagueData = eliteApi.getLeagueData();
    // var leagues = eliteApi.getLeagues();
    console.log("leagueData standings: ",leagueData.standings);
    data.standings = leagueData.standings;
  });