
  'use strict';

  angular.module('eliteApp')

  .controller('teamDetailController', function($scope,$stateParams, $ionicPopup,eliteApi){
    var data = this;
    var leagueData = eliteApi.getLeagueData();

    data.teamId = Number($stateParams.id);
    data.following = false;

    // console.log("leagueData : ", leagueData);
    // console.log("stateParams : ", $stateParams);
    console.log("teamId : ", data.teamId);

    // get team name
    // using lodash libraries for search function
    var divisionTeams = _.chain(leagueData.teams)
                .flatten("divisionTeams")
                .value();
    console.log("teams:",divisionTeams);

    data.teamName = findById(data.teamId,divisionTeams);
    console.log("team name: ", data.teamName);

    // get games using teamId
    data.games = _.chain(leagueData.games)
                  .filter(isTeamInGame)
                  .map(function(item){
                    var isTeam1 = (item.team1Id === data.teamId ? true : false);
                    var opponentName = isTeam1 ? item.team2 : item.team1;
                    var scoreString = getScoreDisplay(isTeam1, item.team1Score, item.team2Score)
                    return {
                      gameId: item.id,
                      opponent: opponentName,
                      time: item.time,
                      location: item.location,
                      locationUrl: item.locationUrl,
                      scoreString: scoreString,
                      homeAway: (isTeam1 ? "vs." : "at")

                    };
                  })
                  .value();
    console.log("games: ", data.games);

    // get team standings
    // lodash helper function
    // data.teamStanding = _.chain(leagueData.standings)
    //                       .flatten("divisionStandings")
    //                       .find({"teamId": data.teamId})
    //                       .value();
    var divStanding = _.chain(leagueData.standings)
                        .flatten("divisionStandings")
                        .value();

    console.log("standings:", divStanding);

    data.teamStanding = getStandingsById(data.teamId, divStanding);
    console.log("team standings: ", data.teamStandings);



    data.toggleFollow = function(){
      // data.following = !data.following;
      if(data.following){
        var confirmPopup = $ionicPopup.confirm({
          title: 'Unfollow?',
          template: 'Are you sure you want to unfollow?'
        });
        confirmPopup.then(function(res){
          if(res){
            data.following = !data.following;
            // console.log("following ?", data.following);
            // if execution goes here then it doesnt go to bottom console log
          }
        });
      }
      else{
        data.following = !data.following;
      }
      // TODO why is this not printing when following is false?
      console.log("following ?", data.following);
    };

    function isTeamInGame(item){
      return item.team1Id === data.teamId || item.team2Id === data.team2Id;
    }

    function getScoreDisplay(isTeam1, team1Score, team2Score){
      if (team1Score && team2Score){
        var teamScore = (isTeam1 ? team1Score : team2Score);
        var opponentScore = (isTeam1 ? team2Score : team1Score);
        // TODO compute correct score
        var winIndicator = teamScore > opponentScore ? "W: " : "L: ";
        return winIndicator + teamScore + "-" + opponentScore;
      }
      else{
        return "";
      }
    }

    // get team name by id
    function findById(id, deepObj){
      var i=0;
      for(i;i<deepObj.length;i++){
        var div = deepObj[i];
        var j=0;
        for(j;j<div.divisionTeams.length;j++){
          var team = div.divisionTeams[j];
          if(team.id === id){
          return team.name ;
          }
        }
      }
    }

    function getStandingsById(id, deepObj){
      var i=0;
      for(i;i<deepObj.length;i++){
        var div = deepObj[i];
        var j=0;
        for(j;j<div.divisionStandings.length;j++){
          var team = div.divisionStandings[j];
          if(team.teamId === id){
            return team ;
          }

        }
      }
    };



  }); // end controller function