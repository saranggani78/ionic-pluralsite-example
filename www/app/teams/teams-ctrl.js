
  'use strict';

  angular.module('eliteApp')

  // using custom service "eliteApi"
  .controller('TeamsCtrl', function(eliteApi){
    var data = this;
    var leagueData = eliteApi.getLeagueData();

    console.log("leagueData teams: ",leagueData.teams);
    data.teams = leagueData.teams;
  });