
  'use strict';

  angular.module('eliteApp')

  // using custom service "eliteApi"
  .controller('LocationsCtrl', function(eliteApi){
    var data = this;
    var leagueData = eliteApi.getLeagueData();
    // var leagues = eliteApi.getLeagues();
    data.locations = leagueData.locations;
    console.log("leagueData locations: ",data.locations);
  });