
  'use strict';

  angular.module('eliteApp')

  // using custom service "eliteApi"
  .controller('GameCtrl', function($state,$stateParams,eliteApi){
    var data = this;
    // var leagueData = eliteApi.getLeagueData();
    var leagueData = eliteApi.getLeagueData();
    // var leagues = eliteApi.getLeagues();
    var gameId = Number($stateParams.id);

    // data.games = leagueData.games;
    // using lodash
    data.game = _.find(leagueData.games,{ "id": gameId});
    console.log("game id: ", gameId);
    console.log("leagueData game: ",data.game);
    console.log("test:", data.game.team2);

  });